#!/bin/bash
##SBATCH --partition=short     ### Partition
#SBATCH --job-name=hello     ### Job Name
#SBATCH --time=1:00:00      ### WallTime
#SBATCH --nodes=4            ### Number of Nodes
#SBATCH --ntasks-per-node=1 ### Number of tasks (MPI processes)
##SBATCH --cpus-per-task=1
#SBATCH --mail-type=BEGIN,END,FAIL,TIME_LIMIT
#SBATCH --mail-user=ryelle@uoregon.edu

echo Hello from `hostname`
 
module load mpi

mpirun -np 8 ./hello
